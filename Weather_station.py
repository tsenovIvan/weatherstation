#!/bin/python

import sys
import datetime
from Adafruit_BME280 import *
from pymongo import MongoClient

sensor = BME280(mode=BME280_OSAMPLE_1)

degrees = sensor.read_temperature()
pascals = sensor.read_pressure()
hectopascals = pascals / 100
humidity = sensor.read_humidity()

#print (sensor.t_fine)
#print to screeen only when verbous
if len(sys.argv) > 1  and sys.argv[1]== "-v":
	print (sensor.t_fine)
	print ('Temp: {0:0.3f} dec C'.format(degrees))
	print ('Pressure: {0:0.2f} hPa'.format(hectopascals))
	print ('Humidity: {0:0.2f} %'.format(humidity))
	print (time.strftime("%H:%M:%S"))
	print (time.strftime("%d/%m/%Y"))

if humidity != 0:
	dbClient = MongoClient()
	dBase = dbClient.weatherstation


	data =	{
			"temp": '{0:0.3f}'.format(degrees),
			"press": '{0:0.2f}'.format(hectopascals),
			"humidity": '{0:0.2f}'.format(humidity),
			"date": datetime.datetime.utcnow(),
			"time": time.strftime("%H:%M:%S")
		}

	result = dBase.indoordata.insert_one(data)
