## Dependencies

1. i2c-tools-git  from https://aur.archlinux.org/packages/i2c-tools-git/
2. python-smbus-git from https://aur.archlinux.org/packages/python-smbus-git/
3. Adafruit_Python_GPIO from https://github.com/adafruit/Adafruit_Python_GPIO
4. Adafruit_BME280.py from https://github.com/adafruit/Adafruit_Python_BME280/
5. pymongo from https://pypi.python.org/pypi/pymongo
