The code here requers an raduino compatible board called BlueDuino  
https://www.tindie.com/products/AprilBrother/blueduino-rev2-  

The code is based on examples and libraries provided by AprilBrother, creator of the BlueDuino  
The code is located here https://github.com/AprilBrother/BlueDuino-Library  
It does not state specific license  
A big, big credit to them !!
